///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Joshua Brewer <brewerj3@hawaii.edu>
/// @date 09_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

#include "gge.h"

double fromGasGallonToJoule( double gasGallon ) {
   return gasGallon / GASOLINE_GALLON_EQ_IN_A_JOULE;
}
double fromJouleToGasGallon( double joule ) {
   return joule * GASOLINE_GALLON_EQ_IN_A_JOULE;
}
