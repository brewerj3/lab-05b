///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Joshua Brewer <brewerj3@hawaii.edu>
/// @date 09_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//
#pragma once
const double GASOLINE_GALLON_EQ_IN_A_JOULE = 1/1.213e8;
const char GAS_GALLON_EQ                   = 'g';

extern double fromGasGallonToJoule( double gasGallon ) ;
extern double fromJouleToGasGallon( double joule ) ;
