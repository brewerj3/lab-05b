///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Joshua Brewer <brewerj3@hawaii.edu>
/// @date 09_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

#include "cat.h"
double fromCatPowerToJoule( double catPower) {
   return catPower * 0;
}
double fromJouleToCatPower( double joule ) {
   return joule * 0;
}
